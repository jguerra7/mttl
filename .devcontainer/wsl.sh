#!/bin/bash

sudo apt-get -y update
sudo apt-get -y install --no-install-recommends apt-utils 2>&1
sudo apt-get -y install sudo ruby-full python3-dev python3-pip gcc g++ git openssh-client

echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc

gem install jekyll bundler:2.1.4 webrick bigdecimal
bundler install