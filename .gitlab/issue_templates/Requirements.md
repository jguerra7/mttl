### Description of new requirement / new work role or specialization / requirement change




### Acceptance Criteria

* [ ] Add the KSAs to the appropriate .json files
* [ ] pipeline must pass all validation checks


/label ~REQS
