# MTTL

### To contribute to the MTTL:
1.  Review the schema to ensure you have the neccesary fields for your contribution, please see the folowing link: https://gitlab.com/90COS/public/mttl/-/wikis/Process/MTTL-Automation-Design#knowledge-skill-ability-and-task-json-files
2.  a) Click the "Issues" button on the left side bar.

	b) Create a new issue by clicking the "New Issue" button.
![Issue](.gitlab/issue_templates/Issue.png)
3.  Select the template that best matches the changes that you are proposing from the "Description" dropdown.

    a) To propose a new workrole, select the "New_Work_Role" template.
    
    b) To propose general changes, select the "General_Contribution" template.
4.  a) Provide a useful title that we can track.

	b) Replace the text wrapped in parentheses with the required information.
	
	c) Provide a Label in the "Labels" dropdown.

Notes: This is intended to gather the contributing information and not how to implement the contributors changes into the