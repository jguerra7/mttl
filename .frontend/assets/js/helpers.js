const cors_anywhere_url = "https://cors-anywhere.herokuapp.com/"

const YELLOW = '#e7de12'
const GREEN = '#58e139'
const BLUE = '#66e0ff'

const eval_no_training = {
    'background-color': YELLOW,
    'color': 'black'
}
const training_and_eval = {
    'background-color': GREEN,
    'color': 'black'
}

const training_no_eval = {
    'background-color': BLUE,
    'color': 'black'
}

function rowStyle(row, index) {
    if (row['training'].length > 0 && row['eval'].length > 0) {
        return { css: training_and_eval };
    }
    else if (row['training'].length > 0) {
        return { css: training_no_eval };
    }
    else if (row['eval'].length > 0) {
        return { css: eval_no_training };
    }
    return {};
}

function setMetrix(id, percent) {
    var i = 0;
    if (i == 0) {
        i = 1;
        var elem = document.getElementById(id);
        var width = 1;
        var id = setInterval(frame, 10);
        function frame() {
            if (width >= percent) {
                clearInterval(id);
                i = 0;
            } else {
                width++;
                elem.style.backgroundColor = getGreenToRed(width);
                elem.style.width = width + "%";
                elem.innerHTML = width + "%";
            }           
        }
    }
}

function getGreenToRed(percent){
    r = percent<50 ? 255 : Math.floor(255-(percent*2-100)*255/100);
    g = percent>50 ? 255 : Math.floor((percent*2)*255/100);
    return 'rgb('+r+','+g+',0)';
}

if (localStorage.getItem('cookieSeen') != 'shown' && localStorage.getItem('consentAgreed') != 'agreed') {
    $('.init_consent').delay(0).fadeIn();
    $('body').css('overflow', 'hidden')
    localStorage.setItem('cookieSeen','shown')
};
$('.close').click(function() {
    $('.init_consent').fadeOut();
    $('body').css('overflow', 'auto')
    localStorage.setItem('consentAgreed','agreed')
})

function http_get_request(url, func) {
    const Http = new XMLHttpRequest();
    Http.open("GET", url);     
    Http.send();

    Http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            func(JSON.parse(Http.response))
        }
        else if (this.status != 200) {
            console.log(url, "returned status: ", this.status);   
        }
    }
}