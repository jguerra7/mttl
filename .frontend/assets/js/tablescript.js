http_get_request("assets/data/metrics/MTTL-metrics.min.json", (response) => {
    setMetrix('tasksBar', response['TASKS']['percent-total']);
    setMetrix('ksaBar',  response['KSA']['percent-total']);
    // setMetrix('wrspecBar', 100);
})

$('.tasksTable').bootstrapTable({
    url: 'assets/data/TASKS.min.json',
    height: 450,
    search: true,
    rowStyle: rowStyle,
    showFullscreen: true,
    showColumns: true,    
    columns: [
        {field: 'ID', title: 'ID', sortable: true},
        {field: 'requirement_src', title: 'Source', sortable: true},
        {field: 'requirement_owner', title: 'Owner', sortable: true},
        // {field: 'parent', title: 'Parent', sortable: true},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'children', title: 'Children KSA&Ts', sortable: true},
        {field: 'Workrole/Specialization', title: 'Work Role/Specialization', sortable: true},
        {field: 'topic', title: 'Topic', sortable: true},
        {field: 'training', title: 'Training Covered', sortable: true},
        {field: 'eval', title: 'Eval Covered', sortable: true}
    ]
})
$('.ksaTable').bootstrapTable({
    url: 'assets/data/KSA.min.json',
    height: 450,
    search: true,
    rowStyle: rowStyle,
    showFullscreen: true,
    showColumns: true,    
    columns: [
        {field: 'ID', title: 'ID', sortable: true},
        {field: 'requirement_src', title: 'Source', sortable: true},
        {field: 'requirement_owner', title: 'Owner', sortable: true},
        {field: 'parent', title: 'Parent KSA&Ts', sortable: true},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'children', title: 'Children KSA&Ts', sortable: true},
        {field: 'Workrole/Specialization', title: 'Work Role/Specialization', sortable: true},
        {field: 'topic', title: 'Topic', sortable: true},
        {field: 'training', title: 'Training Covered', sortable: true},
        {field: 'eval', title: 'Eval Covered', sortable: true}
    ]
})
$('.wrspecTable').bootstrapTable({
    url: 'assets/data/tables/wr_spec.min.json',
    height: 450,
    search: true,
    classes: 'table',
    showFullscreen: true,
    showColumns: true,
      
    columns: [
        {field: 'Workrole/Specialization', title: 'Work Role/Specialization', sortable: true},
        {field: 'KSA_list', title: 'KSA List'}
    ]
})

$('.wrspecTable').on('load-success.bs.table', function (data) { })