http_get_request("../assets/data/metrics/TTL-metrics.min.json", (response) => {
    setMetrix('seeBar', response['SEE-TTL']['percent-total']);
})

$('.seeTable').bootstrapTable({
    url: '../assets/data/tables/SEE-TTL.min.json',
    height: 450,
    search: true,
    rowStyle: rowStyle,
    showFullscreen: true,
    showColumns: true,
    columns: [
        {field: 'ID', title: 'ID', sortable: true},
        {field: 'KSA_ID', title: 'KSA ID', sortable: true},
        {field: 'parent', title: 'Parent', sortable: true},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'children', title: 'Children', sortable: true},
        {field: 'topic', title: 'Topic', sortable: true},
        {field: 'proficiency', title: 'Proficiency', sortable: true},
        {field: 'training', title: 'Training Covered', sortable: true},
        {field: 'eval', title: 'Eval Covered', sortable: true}
    ]
})

$('.seeTable').on('load-success.bs.table', function (data) { })