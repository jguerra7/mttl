# 90 COS Stan/Eval Coverage Map
This site provides basic KSA mapping metrics for the 90 COS workroles.

## Setup your Environment and Contribute
This site uses the Jekyll framework to bundle the site's pages to push to Gitlab Pages.
Reading the following will be helpful...
- [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Jekyll](https://jekyllrb.com/docs/)

### Quick Start Ubuntu
```sh
// Install Ruby
sudo apt-get install ruby-full build-essential zlib1g-dev

// Avoid root user shenanigans
echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrcX
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc

// Install jekyll and bundler
gem install jekyll bundler

// Clone the repo and change directory
git clone https://gitlab.com/90COS/public/mttl-coverage-map.git && cd mttl-coverage-map

// Install all dependencies
bundle install

// Build and start the dev server
jekyll build && jekyll serve -H 0.0.0.0
```

### Quick Start Windows
First you will need to install Ruby for Windows found [here](https://rubyinstaller.org/). Once finish open powershell and run the fillowing.
```sh
// Install jekyll and bundler
gem install jekyll bundler

// Clone the repo and change directory
git clone https://gitlab.com/90COS/public/mttl-coverage-map.git && cd mttl-coverage-map

// Install all dependencies
bundle install

// Build and start the dev server
jekyll build && jekyll serve
```