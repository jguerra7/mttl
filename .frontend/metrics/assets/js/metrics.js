http_get_request("../assets/data/metrics/MTTL-metrics.min.json", (response) => {
    setMetrix('ksa_wte', response['KSA']['percent-total']);
    setMetrix('ksa_wt', response['KSA']['percent-training']);
    setMetrix('ksa_we', response['KSA']['percent-eval']);

    setMetrix('task_wte', response['TASKS']['percent-total']);
    setMetrix('task_wt', response['TASKS']['percent-training']);
    setMetrix('task_we', response['TASKS']['percent-eval']);
})
http_get_request("../assets/data/metrics/TTL-metrics.min.json", (response) => {
    setMetrix('bd_wte', response['basic-dev-TTL']['percent-total']);
    setMetrix('bd_wt', response['basic-dev-TTL']['percent-training']);
    setMetrix('bd_we', response['basic-dev-TTL']['percent-eval']);
    
    setMetrix('bpo_wte', response['basic-po-TTL']['percent-total']);
    setMetrix('bpo_wt', response['basic-po-TTL']['percent-training']);
    setMetrix('bpo_we', response['basic-po-TTL']['percent-eval']);
    
    setMetrix('sdw_wte', response['senior-dev-windows-TTL']['percent-total']);
    setMetrix('sdw_wt', response['senior-dev-windows-TTL']['percent-training']);
    setMetrix('sdw_we', response['senior-dev-windows-TTL']['percent-eval']);
    
    setMetrix('sdl_wte', response['senior-dev-linux-TTL']['percent-total']);
    setMetrix('sdl_wt', response['senior-dev-linux-TTL']['percent-training']);
    setMetrix('sdl_we', response['senior-dev-linux-TTL']['percent-eval']);

    setMetrix('spo_wte', response['senior-po-TTL']['percent-total']);
    setMetrix('spo_wt', response['senior-po-TTL']['percent-training']);
    setMetrix('spo_we', response['senior-po-TTL']['percent-eval']);

    setMetrix('see_wte', response['SEE-TTL']['percent-total']);
    setMetrix('see_wt', response['SEE-TTL']['percent-training']);
    setMetrix('see_we', response['SEE-TTL']['percent-eval']);
})
