http_get_request("../assets/data/metrics/TTL-metrics.min.json", (response) => {
    setMetrix('bdBar', response['basic-dev-TTL']['percent-total']);    
    setMetrix('bpoBar', response['basic-po-TTL']['percent-total']);    
    setMetrix('sdwBar', response['senior-dev-windows-TTL']['percent-total']); 
    setMetrix('sdlBar', response['senior-dev-linux-TTL']['percent-total']);
    setMetrix('spoBar', response['senior-po-TTL']['percent-total']);
})

$('.bdTable').bootstrapTable({
    url: '../assets/data/tables/basic-dev-TTL.min.json',
    height: 450,
    search: true,
    rowStyle: rowStyle,
    showFullscreen: true,
    showColumns: true,
    columns: [
        {field: 'ID', title: 'ID', sortable: true},
        {field: 'KSA_ID', title: 'KSA ID', sortable: true},
        {field: 'parent', title: 'Parent', sortable: true},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'children', title: 'Children', sortable: true},
        {field: 'topic', title: 'Topic', sortable: true},
        {field: 'proficiency', title: 'Proficiency', sortable: true},
        {field: 'training', title: 'Training Covered', sortable: true},
        {field: 'eval', title: 'Eval Covered', sortable: true}
    ]
})
$('.bpoTable').bootstrapTable({
    url: '../assets/data/tables/basic-po-TTL.min.json',
    height: 450,
    search: true,
    rowStyle: rowStyle,
    showFullscreen: true,
    showColumns: true,
    columns: [
        {field: 'ID', title: 'ID', sortable: true},
        {field: 'KSA_ID', title: 'KSA ID', sortable: true},
        {field: 'parent', title: 'Parent', sortable: true},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'children', title: 'Children', sortable: true},
        {field: 'topic', title: 'Topic', sortable: true},
        {field: 'proficiency', title: 'Proficiency', sortable: true},
        {field: 'training', title: 'Training Covered', sortable: true},
        {field: 'eval', title: 'Eval Covered', sortable: true}
    ]
})
$('.sdwTable').bootstrapTable({
    url: '../assets/data/tables/senior-dev-windows-TTL.min.json',
    height: 450,
    search: true,
    rowStyle: rowStyle,
    showFullscreen: true,
    showColumns: true,
    columns: [
        {field: 'ID', title: 'ID', sortable: true},
        {field: 'KSA_ID', title: 'KSA ID', sortable: true},
        {field: 'parent', title: 'Parent', sortable: true},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'children', title: 'Children', sortable: true},
        {field: 'topic', title: 'Topic', sortable: true},
        {field: 'proficiency', title: 'Proficiency', sortable: true},
        {field: 'training', title: 'Training Covered', sortable: true},
        {field: 'eval', title: 'Eval Covered', sortable: true}
    ]
})
$('.sdlTable').bootstrapTable({
    url: '../assets/data/tables/senior-dev-linux-TTL.min.json',
    height: 450,
    search: true,
    rowStyle: rowStyle,
    showFullscreen: true,
    showColumns: true,
    columns: [
        {field: 'ID', title: 'ID', sortable: true},
        {field: 'KSA_ID', title: 'KSA ID', sortable: true},
        {field: 'parent', title: 'Parent', sortable: true},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'children', title: 'Children', sortable: true},
        {field: 'topic', title: 'Topic', sortable: true},
        {field: 'proficiency', title: 'Proficiency', sortable: true},
        {field: 'training', title: 'Training Covered', sortable: true},
        {field: 'eval', title: 'Eval Covered', sortable: true}
    ]
})
$('.spoTable').bootstrapTable({
    url: '../assets/data/tables/senior-po-TTL.min.json',
    height: 450,
    search: true,
    rowStyle: rowStyle,
    showFullscreen: true,
    showColumns: true,
    columns: [
        {field: 'ID', title: 'ID', sortable: true},
        {field: 'KSA_ID', title: 'KSA ID', sortable: true},
        {field: 'parent', title: 'Parent', sortable: true},
        {field: 'description', title: 'Description', class: "header_description", sortable: true},
        {field: 'children', title: 'Children', sortable: true},
        {field: 'topic', title: 'Topic', sortable: true},
        {field: 'proficiency', title: 'Proficiency', sortable: true},
        {field: 'training', title: 'Training Covered', sortable: true},
        {field: 'eval', title: 'Eval Covered', sortable: true}
    ]
})

$('.bdTable').on('load-success.bs.table', function (data) { })
