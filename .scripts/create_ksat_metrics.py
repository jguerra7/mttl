#!/usr/bin/python3

import os, json
from helpers import get_all_json

def main():
    metrics = {}

    root = '.frontend/assets/data'
    paths = ['KSA.min.json', 'TASKS.min.json']

    for path in paths:
        ttl_name = path.split('/')[-1].split('.')[0]
        metrics[ttl_name] = {
            'percent-total': 0, 
            'percent-training': 0, 
            'percent-eval': 0, 
            'ksas-without-training': [],
            'ksas-without-eval': []
        }

        without_training_set = []
        without_eval_set = []
        with_training_set = set()
        with_eval_set = set()

        with open(f'{root}/{path}', 'r') as ttl_file:
            ttl_data = json.load(ttl_file)
            for ttl in ttl_data:
                if len(ttl['training']) == 0: without_training_set.append(ttl['ID'])
                else: with_training_set.add(ttl['ID'])
                if len(ttl['eval']) == 0: without_eval_set.append(ttl['ID'])
                else: with_eval_set.add(ttl['ID'])

            total = len(ttl_data)
            with_training = len(with_training_set)
            with_eval = len(with_eval_set)
            with_both = len(with_training_set.intersection(with_eval_set))

            metrics[ttl_name]['percent-total'] = (with_both / total) * 100
            metrics[ttl_name]['percent-training'] = (with_training / total) * 100
            metrics[ttl_name]['percent-eval'] = (with_eval / total) * 100
            metrics[ttl_name]['ksas-without-training'] = without_training_set
            metrics[ttl_name]['ksas-without-eval'] = without_eval_set

    os.makedirs(f'{root}/metrics', exist_ok=True)
    with open(f'{root}/metrics/MTTL-metrics.min.json', 'w') as metrics_file:
        json.dump(metrics, metrics_file, sort_keys=False, separators=(',', ':'))

if __name__ == "__main__":
    main()