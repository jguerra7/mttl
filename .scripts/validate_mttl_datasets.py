#!/usr/bin/python3

import json

def errorprint(txt, data, path):
    print(f'No {txt} present in {path}')
    print(data)
    print()

def main():
    err = 0
    root = '.frontend/assets/data'
    paths = ['KSA.min.json', 'TASKS.min.json']

    for path in paths:
        with open(f'{root}/{path}', 'r') as mttlfile:
            mttldata = json.load(mttlfile)
            for item in mttldata:
                if len(item['ID']) == 0:
                    errorprint('ID', item, path)
                    err = 1
                if len(item['description']) == 0:
                    errorprint('description', item, path)
                    err = 1
                if len(item['topic']) == 0:
                    errorprint('topic', item, path)
                    err = 1
                if len(item['requirement_src']) == 0:
                    errorprint('requirement_src', item, path)
                    err = 1
                if len(item['requirement_owner']) == 0:
                    errorprint('requirement_owner', item, path)
                    err = 1
                if len(item['Workrole/Specialization']) == 0:
                    errorprint('Workrole/Specialization', item, path)
                    err = 1

    if err == 1:
        exit(1)

if __name__ == "__main__":
    main()