#!/usr/bin/python3

import json
from helpers import get_all_json

def errorprint(txt, data, path):
    print(f'No {txt} present in {path}')
    print(data)
    print()

def main():
    err = 0
    root = '.frontend/assets/data/tables'
    paths = get_all_json(root, 'TTL', [])

    for path in paths:
        with open(path, 'r') as mttlfile:
            mttldata = json.load(mttlfile)
            for item in mttldata:
                if len(item['ID']) == 0:
                    errorprint('ID', item, path)
                    err = 1
                if len(item['KSA_ID']) == 0:
                    errorprint('KSA_ID', item, path)
                    err = 1
                if len(item['description']) == 0:
                    errorprint('description', item, path)
                    err = 1
                if len(item['topic']) == 0:
                    errorprint('topic', item, path)
                    err = 1
                if len(item['requirement_src']) == 0:
                    errorprint('requirement_src', item, path)
                    err = 1
                if len(item['requirement_owner']) == 0:
                    errorprint('requirement_owner', item, path)
                    err = 1
                if len(item['proficiency']) == 0:
                    errorprint('proficiency', item, path)
                    err = 1

    if err == 1:
        exit(1)

if __name__ == "__main__":
    main()