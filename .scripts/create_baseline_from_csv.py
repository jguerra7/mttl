#!/usr/bin/python3

import json, csv, os

basic_dev_count = 1
basic_po_count = 1
senior_dev_linux_count = 1
senior_dev_windows_count = 1
senior_po_count = 1
see_count = 1

task_count = 1
knowledge_count = 1
ability_count = 1
skill_count = 1

basic_dev_dict = {}
basic_po_dict = {}
senior_dev_linux_dict = {}
senior_dev_windows_dict = {}
senior_po_dict = {}
see_dict = {}

task_dict = {}
knowledge_dict = {}
ability_dict = {}
skill_dict = {}

parent_set = set()
parent_list = []

ksa_id_holder = ''
skill_area_id_holder = ''

def bind_relationships():
    global parent_list
    
    for parent in parent_list:
        if 'K' in parent[0]:
            knowledge_dict[parent[0]]['children'] = parent[2]
        if 'S' in parent[0]:
            skill_dict[parent[0]]['children'] = parent[2]
        if 'A' in parent[0]:
            ability_dict[parent[0]]['children'] = parent[2]

        for child in parent[2]:
            if 'K' in child:
                knowledge_dict[child]['parent'].append(parent[0])
            if 'S' in child:
                skill_dict[child]['parent'].append(parent[0])
            if 'A' in child:
                ability_dict[child]['parent'].append(parent[0]) 

def parent_child_relationships(ksa, item_parent_list):
    global parent_set, parent_list, ksa_id_holder
    global knowledge_count, ability_count, skill_count, task_count
    global knowledge_dict, ability_dict, skill_dict, task_dict

    parent_set_len = len(parent_set)
    parent_set.add(ksa['TASK'])
    if len(parent_set) > parent_set_len:
        parent = [ksa_id_holder, ksa['TASK'], []]
        if 'T' in ksa_id_holder:
            task_dict[ksa_id_holder] = {}
            task_dict[ksa_id_holder]['old_UID'] = ksa['UID']
            task_dict[ksa_id_holder]['description'] = ksa['TASK']
            task_dict[ksa_id_holder]['parent'] = []
            task_dict[ksa_id_holder]['children'] = []
            task_dict[ksa_id_holder]['topic'] = ksa['Topic/Language/Tool/Application']
            task_dict[ksa_id_holder]['requirement_src'] = [f'{ksa["Req Src"]} {ksa["Req Src ID"]}']
            task_dict[ksa_id_holder]['requirement_owner'] = ksa['Requirement Owner(s)']
            task_dict[ksa_id_holder]['comments'] = ksa['COMMENTS']
            task_dict[ksa_id_holder]['training'] = []
            task_dict[ksa_id_holder]['eval'] = []
            task_count = task_count + 1
            ksa_id_holder = f'{ksa_id_holder[:-4]}{task_count:04d}'
        if 'K' in ksa_id_holder:
            knowledge_dict[ksa_id_holder] = {}
            knowledge_dict[ksa_id_holder]['old_UID'] = ksa['UID']
            knowledge_dict[ksa_id_holder]['description'] = ksa['TASK']
            knowledge_dict[ksa_id_holder]['parent'] = []
            knowledge_dict[ksa_id_holder]['children'] = []
            knowledge_dict[ksa_id_holder]['topic'] = ksa['Topic/Language/Tool/Application']
            knowledge_dict[ksa_id_holder]['requirement_src'] = [f'{ksa["Req Src"]} {ksa["Req Src ID"]}']
            knowledge_dict[ksa_id_holder]['requirement_owner'] = ksa['Requirement Owner(s)']
            knowledge_dict[ksa_id_holder]['comments'] = ksa['COMMENTS']
            knowledge_dict[ksa_id_holder]['training'] = []
            knowledge_dict[ksa_id_holder]['eval'] = []
            knowledge_count = knowledge_count + 1
            ksa_id_holder = f'{ksa_id_holder[:-4]}{knowledge_count:04d}'
        if 'S' in ksa_id_holder:
            skill_dict[ksa_id_holder] = {}
            skill_dict[ksa_id_holder]['old_UID'] = ksa['UID']
            skill_dict[ksa_id_holder]['description'] = ksa['TASK']
            skill_dict[ksa_id_holder]['parent'] = []
            skill_dict[ksa_id_holder]['children'] = []
            skill_dict[ksa_id_holder]['topic'] = ksa['Topic/Language/Tool/Application']
            skill_dict[ksa_id_holder]['requirement_src'] = [f'{ksa["Req Src"]} {ksa["Req Src ID"]}']
            skill_dict[ksa_id_holder]['requirement_owner'] = ksa['Requirement Owner(s)']
            skill_dict[ksa_id_holder]['comments'] = ksa['COMMENTS']
            skill_dict[ksa_id_holder]['training'] = []
            skill_dict[ksa_id_holder]['eval'] = []
            skill_count = skill_count + 1
            ksa_id_holder = f'{ksa_id_holder[:-4]}{skill_count:04d}'
        if 'A' in ksa_id_holder:
            ability_dict[ksa_id_holder] = {}
            ability_dict[ksa_id_holder]['old_UID'] = ksa['UID']
            ability_dict[ksa_id_holder]['description'] = ksa['TASK']
            ability_dict[ksa_id_holder]['parent'] = []
            ability_dict[ksa_id_holder]['children'] = []
            ability_dict[ksa_id_holder]['topic'] = ksa['Topic/Language/Tool/Application']
            ability_dict[ksa_id_holder]['requirement_src'] = [f'{ksa["Req Src"]} {ksa["Req Src ID"]}']
            ability_dict[ksa_id_holder]['requirement_owner'] = ksa['Requirement Owner(s)']
            ability_dict[ksa_id_holder]['comments'] = ksa['COMMENTS']
            ability_dict[ksa_id_holder]['training'] = []
            ability_dict[ksa_id_holder]['eval'] = []
            ability_count = ability_count + 1
            ksa_id_holder = f'{ksa_id_holder[:-4]}{ability_count:04d}'
        parent[2].append(ksa_id_holder)
        parent_list.append(parent)
    else:
        for parent in parent_list:
            if ksa['TASK'] in parent[1]:
                parent[2].append(ksa_id_holder)


def create_skill_area_obj(ksa, skill_area_key):
    global ksa_id_holder
    
    item = {}
    item['KSA'] = ksa_id_holder
    item['proficiency'] = ksa[skill_area_key]
    return item

def create_ksa_obj(ksa):
    global parent_list, ksa_id_holder
    item = {}
    item['old_UID'] = ksa['UID']
    item['parent'] = []
    item['children'] = []
    
    if len(ksa['SUB TASK']) > 0:
        parent_child_relationships(ksa, item['parent'])
        item['description'] = ksa['SUB TASK']
    else:
        item['description'] = ksa['TASK']
    
    item['topic'] = ksa['Topic/Language/Tool/Application']
    item['requirement_src'] = [f'{ksa["Req Src"]} {ksa["Req Src ID"]}']
    item['requirement_owner'] = ksa['Requirement Owner(s)']
    item['comments'] = ksa['COMMENTS']
    item['training'] = []
    item['eval'] = []

    return item

def identify_prof(ksa):
    id = None
    if 'JCT&CS' in ksa['Req Src'] and 'KSA' not in ksa['Req Src ID']:
        id = 'T'
    elif ksa['Basic Dev'].isdigit() or ksa['Sdev-Win'].isdigit() or ksa['Sdev-Lin'].isdigit() or ksa['SEE'].isdigit() or ksa['Basic PO'].isdigit() or ksa['Senior PO'].isdigit():
        id = 'A'
    else:
        id = 'K'
    return id

def identify_skill_area(ksa):
    skill_areas = []
    if len(ksa['Basic Dev']) > 0:
        skill_areas.append('BD')
    if len(ksa['Sdev-Win']) > 0:
        skill_areas.append('SDW')
    if len(ksa['Sdev-Lin']) > 0:
        skill_areas.append('SDL')
    if len(ksa['SEE']) > 0:
        skill_areas.append('SEE')
    if len(ksa['Basic PO']) > 0:
        skill_areas.append('BPO')
    if len(ksa['Senior PO']) > 0:
        skill_areas.append('SPO')
    return skill_areas

def create_ksa_dicts(leading_id, ksa_sa_list, ksa):
    global knowledge_count, ability_count, skill_count, task_count
    global knowledge_dict, ability_dict, skill_dict, task_dict
    global basic_dev_count, basic_po_count, senior_dev_linux_count, senior_dev_windows_count, senior_po_count, see_count
    global basic_dev_dict, basic_po_dict, senior_dev_linux_dict, senior_dev_windows_dict, senior_po_dict, see_dict

    global ksa_id_holder
    global skill_area_id_holder


    if 'T' in leading_id:
        ksa_id_holder = f'{leading_id}{task_count:04d}'
        item = create_ksa_obj(ksa)
        task_dict[ksa_id_holder] = item
        task_count = task_count + 1    
    if 'K' in leading_id:
        ksa_id_holder = f'{leading_id}{knowledge_count:04d}'
        item = create_ksa_obj(ksa)
        knowledge_dict[ksa_id_holder] = item
        knowledge_count = knowledge_count + 1
    if 'S' in leading_id:
        ksa_id_holder = f'{leading_id}{skill_count:04d}'
        item = create_ksa_obj(ksa)
        skill_dict[ksa_id_holder] = item
        skill_count = skill_count + 1
    if 'A' in leading_id:
        ksa_id_holder = f'{leading_id}{ability_count:04d}'
        item = create_ksa_obj(ksa)
        ability_dict[ksa_id_holder] = item
        ability_count = ability_count + 1

    for skill_area in ksa_sa_list:
        if 'BD' in skill_area:
            skill_area_id_holder = f'{skill_area}{basic_dev_count:04d}'
            basic_dev_dict[skill_area_id_holder] = create_skill_area_obj(ksa, 'Basic Dev')
            basic_dev_count = basic_dev_count + 1
        if 'BPO' in skill_area:
            skill_area_id_holder = f'{skill_area}{basic_po_count:04d}'
            basic_po_dict[skill_area_id_holder] = create_skill_area_obj(ksa, 'Basic PO')
            basic_po_count = basic_po_count + 1
        if 'SDW' in skill_area:
            skill_area_id_holder = f'{skill_area}{senior_dev_windows_count:04d}'
            senior_dev_windows_dict[skill_area_id_holder] = create_skill_area_obj(ksa, 'Sdev-Win')
            senior_dev_windows_count = senior_dev_windows_count + 1
        if 'SDL' in skill_area:
            skill_area_id_holder = f'{skill_area}{senior_dev_linux_count:04d}'
            senior_dev_linux_dict[skill_area_id_holder] = create_skill_area_obj(ksa, 'Sdev-Lin')
            senior_dev_linux_count = senior_dev_linux_count + 1
        if 'SPO' in skill_area:
            skill_area_id_holder = f'{skill_area}{senior_po_count:04d}'
            senior_po_dict[skill_area_id_holder] = create_skill_area_obj(ksa, 'Senior PO')
            senior_po_count = senior_po_count + 1
        if 'SEE' in skill_area:
            skill_area_id_holder = f'{skill_area}{see_count:04d}'
            see_dict[skill_area_id_holder] = create_skill_area_obj(ksa, 'SEE')
            see_count = see_count + 1
            

def main():
    global knowledge_dict, ability_dict, skill_dict, task_dict
    global basic_dev_dict, basic_po_dict, senior_dev_linux_dict, senior_dev_windows_dict, senior_po_dict, see_dict

    with open('MTTL.csv', 'r') as mttlcsv:
        mttl_reader = csv.DictReader(mttlcsv)
        for row in mttl_reader:
            create_ksa_dicts(identify_prof(row), identify_skill_area(row), row)

    bind_relationships()
    
    with open('requirements/TASKS.json', 'w') as fd:
        json.dump(task_dict, fd, sort_keys=False, indent=4)
    with open('requirements/KNOWLEDGE.json', 'w') as fd:
        json.dump(knowledge_dict, fd, sort_keys=False, indent=4)
    with open('requirements/ABILITIES.json', 'w') as fd:
        json.dump(ability_dict, fd, sort_keys=False, indent=4)
    with open('requirements/SKILLS.json', 'w') as fd:
        json.dump(skill_dict, fd, sort_keys=False, indent=4)
                    
    with open('workroles/basic-dev-TTL.json', 'w') as fd:
        json.dump(basic_dev_dict, fd, sort_keys=False, indent=4)
    with open('workroles/basic-po-TTL.json', 'w') as fd:
        json.dump(basic_po_dict, fd, sort_keys=False, indent=4)
    with open('workroles/senior-windows-dev-TTL.json', 'w') as fd:
        json.dump(senior_dev_windows_dict, fd, sort_keys=False, indent=4)
    with open('workroles/senior-linux-dev-TTL.json', 'w') as fd:
        json.dump(senior_dev_linux_dict, fd, sort_keys=False, indent=4)
    with open('workroles/senior-po-TTL.json', 'w') as fd:
        json.dump(senior_po_dict, fd, sort_keys=False, indent=4)
    with open('specializations/SEE-TTL.json', 'w') as fd:
        json.dump(see_dict, fd, sort_keys=False, indent=4)



if __name__ == "__main__":
    main()
