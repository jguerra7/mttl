#!/usr/bin/python3

import os
import sys
import json
import helpers
import argparse
import jsonschema

avoid_list_default = None
avoid_list_desc = "A list of files to ignore when building the working file list. " \
                  "The default is '{0}'.".format(avoid_list_default)
description = "This validates one or more requirement files & the TRN_ID/EVL_ID within each requirement file."
evl_urls_file_default = "EVL.json"
evl_urls_file_desc = "The absolute or relative path to the file name containing the evaluation repo URLs. " \
                     "The default is {0}.".format(evl_urls_file_default)
keyword_default = ".json"
keyword_desc = "A keyword to specify the returned file paths for processing. " \
               "The default is '{0}'.".format(keyword_default)
log_file_default = "ValidateTrnEvlIdLog.json"
log_file_desc = "The name of the log file; the default is '{0}'".format(log_file_default)
out_dir_default = "."
out_dir_desc = "The directory to store the output 'log_file'. Note, the directory is created if it doesn't exist. " \
               "By default files are written to {0}.".format(log_file_default, out_dir_default)
start_dir_default = "./requirements"
start_dir_desc = "This is the starting directory to the requirements. The default is '{0}'.".format(start_dir_default)
trn_urls_file_default = "TRN.json"
trn_urls_file_desc = "The absolute or relative path to the file name containing the training repo URLs. " \
                     "The default is {0}.".format(trn_urls_file_default)

parser = argparse.ArgumentParser(description=description)
parser.add_argument("-a", "--avoid_list", type=list, default=avoid_list_default, help=avoid_list_desc)
parser.add_argument("-e", "--eval_urls_file", type=str, default=evl_urls_file_default, help=evl_urls_file_desc)
parser.add_argument("-k", "--keyword", type=str, default=keyword_default, help=keyword_desc)
parser.add_argument("-l", "--log_file", type=str, default=log_file_default, help=log_file_desc)
parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
parser.add_argument("-s", "--start_dir", type=str, default=start_dir_default, help=start_dir_desc)
parser.add_argument("-t", "--train_urls_file", type=str, default=trn_urls_file_default, help=trn_urls_file_desc)

log = []
# This represents the general keys/values required in a requirement (K, S, A or T) JSON file
req_schema = {
    "title": "Schema for a MTTL requirement JSON file.",
    "definitions": {
        "trn_eval": {
            "type": "array",
            "items": {
                "type": "array",
                "items": {"type": "string", "minLength": 1},
                "minItems": 5,
                "maxItems": 5
            },
            "minItems": 0
        }
    },
    "type": "object",
    "patternProperties": {
        "^[A-Za-z]+[0-9]{4,}$": {
            "type": "object",
            "properties": {
                "description": {"type": "string", "minLength": 1},
                "parent": {
                    "type": "array",
                    "items": {"type": "string", "minLength": 1},
                    "minItems": 0
                },
                "children": {
                    "type": "array",
                    "items": {"type": "string", "minLength": 1},
                    "minItems": 0
                },
                "topic": {"type": "string", "minLength": 0},
                "requirement_src": {
                    "type": "array",
                    "items": {"type": "string", "minLength": 0},
                    "minItems": 0
                },
                "requirement_owner": {"type": "string", "minLength": 0},
                "comments": {"type": "string", "minLength": 0},
                "training": {"$ref": "#/definitions/trn_eval"},
                "eval": {"$ref": "#/definitions/trn_eval"}
            },
            "required": [
                "description",
                "parent",
                "children",
                "topic",
                "requirement_src",
                "requirement_owner",
                "comments",
                "training",
                "eval"
            ]
        }
    },
    "minProperties": 0,
    "additionalProperties": False
}


def validate_trn_evl_ids(valid_ids: list, ids_list: list, id_type: str) -> list:
    '''
    Purpose: This function takes the value of the 'training' or 'eval' key within each requirement and validates the
    value of the TRN_EVL_ID against the contents of the TRN.json/EVL.json keys.
    :param valid_ids: This is a list of valid IDs for comparison.
    :param ids_list: This is the list of lists that need each TRN_EVL_ID validated.
    :param id_type: This designates whether this is 'training' or 'eval'.
    :return: Success = []; otherwise, a list of errors
    '''
    errs = []
    # To capture various errors while parsing data within the dictionary
    try:
        # Assumes the TRN/EVL_ID is first in the list; if it's not then this won't validate properly
        # Also, this currently skips cases where the list is empty
        # Since we have a list of lists
        for sub_list in ids_list:
            if len(sub_list) > 0 and sub_list[0] not in valid_ids:
                name = os.path.basename(sub_list[-1]).replace(".rel-link.json", "")
                errs.append("{0} ID mismatch in file '{1}': got '{2}' but expected one of '{3}'.".format(
                    id_type.upper(), name, sub_list[0], valid_ids))
    except (ValueError, KeyError, TypeError) as err:
        errs.append("Error validating '{0}' IDs: {1}".format(id_type, err))
    return errs


def validate_req_file(valid_trn_ids: list, valid_evl_ids: list, req_data: dict, req_file: str) -> list:
    '''
    Purpose: This function validates a single requirement JSON file against its expected schema as well as other
    desired value checks.
    :param valid_trn_ids: A list of valid TRN_IDs for comparison
    :param valid_evl_ids: A list of valid EVL_IDs for comparison
    :param req_data: A dictionary representing a specific requirement JSON file
    :param req_file: The name of the requirement JSON file
    :return: Success = []; otherwise, a list of errors
    '''
    req_log = []
    trn_key = "training"
    evl_key = "eval"
    req_id = ""

    try:
        # Allow collecting all validation errors by using separate try blocks
        jsonschema.validate(instance=req_data, schema=req_schema)  # Check requirement file generally
    except jsonschema.ValidationError as e:
        req_log.append("Validation Error(s) in {0}: {1}".format(os.path.basename(req_file), e))
    try:
        # This checks the value of the TRN_EVL_IDs to be sure they're valid
        for req_id, req_info in req_data.items():
            errs = []  # Lump all errors for this ID into one log list item.
            # Validate the trn_key IDs within each requirement
            errs += validate_trn_evl_ids(valid_trn_ids, req_info[trn_key], trn_key)
            # Validate the evl_key IDs within each requirement
            errs += validate_trn_evl_ids(valid_evl_ids, req_info[evl_key], evl_key)
            if len(errs) > 0:
                req_log.append("Error(s) in '{0}': {1}".format(req_id, errs))
    except (ValueError, KeyError, TypeError) as err:
        req_log.append("Error while parsing '{0}' ID '{1}': {2}".format(os.path.basename(req_file), req_id, err))
    return req_log


def main() -> int:
    '''
    Purpose: This is responsible for collecting all requested input as well as valid data for comparison in order to
    validate each requirement file within the MTTL. Error statements are kept in a list called log.
    :return: Success = 0; errors = 1
    '''
    global args, log
    trn_ids = {}
    evl_ids = {}

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    if args.avoid_list is None:
        args.avoid_list = []

    if not os.path.exists(args.train_urls_file):
        log.append("{0} was not found. Please check the path and try again.".format(args.train_urls_file))
        return 1

    if not os.path.exists(args.eval_urls_file):
        log.append("{0} was not found. Please check the path and try again.".format(args.eval_urls_file))
        return 1

    try:
        with open(args.train_urls_file, "r") as trn_fp:
            trn_ids = json.load(trn_fp)
    except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError) as err:
        log.append("Unable to load JSON data in {0}: {1}.".format(args.train_urls_file, err))
        return 1

    try:
        with open(args.eval_urls_file, "r") as evl_fp:
            evl_ids = json.load(evl_fp)
    except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError) as err:
        log.append("Unable to load JSON data in {0}: {1}.".format(args.train_urls_file, err))
        return 1

    req_files = helpers.get_all_json(args.start_dir, args.keyword, args.avoid_list)

    for req_file in req_files:
        try:
            with open(req_file, "r") as req_fp:
                req_data = json.load(req_fp)
        except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError) as err:
            log.append("Unable to load JSON data in {0}: {1}.".format(req_file, err))
        else:
            # Validate this requirement JSON only if the json.load was successful
            log += validate_req_file(list(trn_ids.keys()), list(evl_ids.keys()), req_data, req_file)

    if len(log) > 0:
        return 1
    else:
        return 0


if __name__ == "__main__":
    args = parser.parse_args()
    status = main()
    if status == 0:
        msg = "All requirements are valid."
        print(msg)
        log.append(msg)
    else:
        print("Check {0} for validation errors.".format(os.path.join(args.out_dir, args.log_file)))

    with open(os.path.join(args.out_dir, args.log_file), "w") as log_fp:
        json.dump(log, log_fp, indent=4)

    sys.exit(status)
