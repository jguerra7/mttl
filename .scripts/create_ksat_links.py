#!/usr/bin/python3

import os, json

def get_all_json(root, avoid_list):
    json_list = []

    for dirName, subdirlist, filelist in os.walk(root):
        for fname in filelist:
            if '.json' in fname and fname not in avoid_list:
                json_list.append(os.path.join(dirName, fname))
    return json_list

def main():
    ksa_paths = get_all_json('./requirements', [])
    ksa_data = None

    for ksa_path in ksa_paths:
        with open(ksa_path, 'r+') as ksa_file:
            links = {}
            ksa_data = json.load(ksa_file)
            for key in ksa_data.keys():
                if len(ksa_data[key]['training']) == 0 and len(ksa_data[key]['eval']) == 0:
                    continue
                links[key] = {'training': [],'eval':[]}
                if len(ksa_data[key]['training']) > 0:
                    for item in ksa_data[key]['training']:
                        value = f'{item[0]}: {item[1]} @ {item[2]} on {item[3]}'
                        if len(item) > 3:
                            links[key]['training'].append(f'<a target="_blank" rel="noopener noreferrer" href="{item[4]}">{value}</a>')
                        else:
                            links[key]['training'].append(value)
                if len(ksa_data[key]['eval']) > 0:
                    for item in ksa_data[key]['eval']:
                        value = f'{item[0]}: {item[1]} @ {item[2]} on {item[3]}'
                        if len(item) > 3:
                            links[key]['eval'].append(f'<a target="_blank" rel="noopener noreferrer" href="{item[4]}">{value}</a>')
                        else:
                            links[key]['eval'].append(value)
            
            with open(ksa_path, 'w') as tmp:
                for key in links.keys():
                    if len(ksa_data[key]['training']) > 0:
                        ksa_data[key]['training'] = links[key]['training']
                    if len(ksa_data[key]['eval']) > 0:
                        ksa_data[key]['eval'] = links[key]['eval']
                        
                ksa_file.seek(0)
                json.dump(ksa_data, tmp, sort_keys=False, indent=4)

if __name__ == "__main__":
    main()