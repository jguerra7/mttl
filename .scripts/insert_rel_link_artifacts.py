#!/usr/bin/python3

import json, os
import requests
from helpers import get_all_json

error = 0
log_str = ""
artifact_url = "/-/jobs/artifacts/master/raw/data.rel-link.json?job=SaveJsons"
json_list = ["TRN.json", "EVL.json"]

ksa_data = None

def request_artifact(url):
    req = None
    try: req = requests.get(f'{url}/{artifact_url}')
    except: 
        print(f'Problem with HTTP request: {url}')
        exit(1)
    return req

def change_data_from_key(mat_key, url, rl_dict, key, prof, rl_key, ksa_path):
    global error
    global ksa_data
    global log_str

    # Get the specific KSA dictionary
    try: ksa_dict = ksa_data[key]
    except:
        error = 1
        log_str = log_str + f'{rl_key} links to {key} but {key} does not exist\n'
        return error
    # Get the specific KSA Dictionary material list (eval or training)
    try: mat_list = ksa_dict[mat_key]
    except:
        error = 1
        log_str = log_str + f'There is no {mat_key} list in {key}\n'
        return error
    
    # Assign training/eval ID
    try: TRN_EVL_ID = rl_dict['TRN_EVL_ID']
    except:
        error = 1
        log_str = log_str + f'There is no TRN_EVL_ID present in {rl_key}\n'
        return error
    # Assign name
    try: NAME = rl_dict['NAME']
    except:
        error = 1
        log_str = log_str + f'There is no NAME present in {rl_key}\n'
        return error
    # Assign network
    try: NETWORK = rl_dict['NETWORK']
    except:
        error = 1
        log_str = log_str + f'There is no NETWORK present in {rl_key}\n'
        return error
    # Assign rel-path
    try: REL_PATH = '/'.join(rl_dict['REL_PATH'].split('/')[:-1])
    except:
        error = 1
        log_str = log_str + f'There is no REL_PATH present in {rl_key}\n'
        return error

    # Append the rel-link info to material list
    try: 
        mat_list.append([TRN_EVL_ID, NAME, prof, NETWORK, f'{url}/-/tree/master/{REL_PATH}'])
    except:
        error = 1
        log_str = log_str + f'There was a problem appending to {key}\'s {mat_key} list\n'
        return error

    return 0

# rl = rel_link
def insert_rel_link_info(rl_data, url, mat_key, rl_key):
    global error
    global ksa_data
    global log_str

    url = url.replace('.git', '')
    ksa_paths = get_all_json('./requirements', '.json', [])

    for ksa_path in ksa_paths:
        with open(ksa_path, 'r') as ksafile:
            ksa_data = json.load(ksafile)

        for rl_dict in rl_data:
            for req_grp in rl_dict['KSATs']:
                # check if the KSA has a PROF_ID
                if len(req_grp[1]) > 0:
                    change_data_from_key(mat_key, url, rl_dict, req_grp[0], req_grp[1], rl_key, ksa_path)
                else:
                    error = 1
                    rel_path = rl_dict['REL_PATH']
                    log_str = log_str + f'{req_grp[0]} proficiency in {rel_path} missing!\n'

        with open(ksa_path, 'w') as ksafile:
            json.dump(ksa_data, ksafile, sort_keys=False, indent=4)


def main():
    global error
    for path in json_list:
        mat_key = 'eval' if 'EVL' in path else 'training'
        
        req = None
        url_data = None
        with open(path, 'r') as datafile:
            url_data = json.load(datafile)
        
        for key in url_data.keys():
            url = None
            try: 
                url = url_data[key]['repo-url']
                req = request_artifact(url)
                insert_rel_link_info(req.json(), url, mat_key, key)
            except: continue

    if error:
        with open('./requirements/insert_rel_link_artifacts.log', 'w+') as log_file:
           log_file.write(log_str)
        print(f'There are errors present. Read the \'requirements/insert_rel_link_artifacts.log\' artifact file')

if __name__ == "__main__":
    main()