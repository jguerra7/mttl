#!/usr/bin/python3

import os, json

error = 0
log_str = ""
ttl_dirs = ['./workroles', './specializations']

def append_wr_spec(ksat_list, ksa_id, wr_spec):
    try:
        if wr_spec not in ksat_list[ksa_id]['Workrole/Specialization']:
            ksat_list[ksa_id]['Workrole/Specialization'].append(wr_spec)
    except:
        ksat_list[ksa_id]['Workrole/Specialization'] = [wr_spec]


def get_wr_spec(ttl):
    wr_spec = ' '.join(ttl.split('/')[-1].split('-')[:-1]).title()
    if wr_spec == 'See': wr_spec = 'SEE'
    elif wr_spec == 'Basic Po': wr_spec = 'Basic PO'
    elif wr_spec == 'Senior Po': wr_spec = 'Senior PO'

    return wr_spec


def generate_ksa_obj(ksa_data, key):
    data = {}

    data['ID'] = key
    data['parent'] = ', '.join(ksa_data[key]['parent'])
    data['description'] = ksa_data[key]['description']
    data['children'] = ', '.join(ksa_data[key]['children'])
    data['topic'] = ksa_data[key]['topic']
    data['requirement_src'] = ', '.join(ksa_data[key]['requirement_src'])
    data['requirement_owner'] = ksa_data[key]['requirement_owner']
    data['Workrole/Specialization'] = []
    data['training'] = '\n'.join(ksa_data[key]['training'])
    data['eval'] = '\n'.join(ksa_data[key]['eval'])

    return data


def main():
    global log_str
    global error
    root = '.frontend/assets/data'
    ksas = {}
    tasks = {}
    ksadata = {}
    ttl_list = []

    with open('requirements/TASKS.json', 'r') as tasksfile:
        ksadata = json.load(tasksfile)
    with open('requirements/KNOWLEDGE.json', 'r') as knowledgefile:
        ksadata = {**json.load(knowledgefile), **ksadata}
    with open('requirements/SKILLS.json', 'r') as skillfile:
        ksadata = {**json.load(skillfile), **ksadata}
    with open('requirements/ABILITIES.json', 'r') as abilityfile:
        ksadata = {**json.load(abilityfile), **ksadata}


    for key in ksadata.keys():
        if 'T' not in key:
            ksas[key] = generate_ksa_obj(ksadata, key)
        else:
            tasks[key] = generate_ksa_obj(ksadata, key)

    rootdir = '.'
    for dirName, subdirlist, filelist in os.walk(rootdir):
        if dirName in ttl_dirs:
            for fname in filelist:
                ttl_list.append(os.path.abspath(f'{dirName}/{fname}'))
    for ttl in ttl_list:
        with open(ttl, 'r') as ttlfile:
            ttldata = json.load(ttlfile)
            for key in ttldata.keys():
                ksa_id = ttldata[key]['KSA']
                try:
                    wr_spec = get_wr_spec(ttl)
                    if 'T' not in ksa_id:
                        append_wr_spec(ksas, ksa_id, wr_spec)
                    else:
                        append_wr_spec(tasks, ksa_id, wr_spec)
                except:
                    error = 1
                    log_str = log_str + f'{ksa_id} not found in KSAT json: {key} in {ttl}\n'

    for key in ksas.keys():
        ksas[key]['Workrole/Specialization'] = ', '.join(ksas[key]['Workrole/Specialization'])
    for key in tasks.keys():
        tasks[key]['Workrole/Specialization'] = ', '.join(tasks[key]['Workrole/Specialization'])

    os.makedirs(root, exist_ok=True)
    with open(f'{root}/KSA.min.json', 'w') as mttfile:
        json.dump(list(ksas.values()), mttfile, sort_keys=False, separators=(',', ':'))
    with open(f'{root}/TASKS.min.json', 'w') as mttfile:
        json.dump(list(tasks.values()), mttfile, sort_keys=False, separators=(',', ':'))

    if error:
        with open(f'{root}/create_ksat_dataset.log', 'w+') as log_file:
            log_file.write(log_str) 
        print(f'There are errors present. Read the \'{root}/create_ksat_dataset.log\' artifact file')


if __name__ == "__main__":    main()