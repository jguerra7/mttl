#!/usr/bin/python3

import json, os

error = 0
log_str = ""
ttl_dirs = ['./workroles', './specializations']

def get_wr_spec(ttl):
    wr_spec = ' '.join(ttl.split('/')[-1].split('-')[:-1]).title()
    if wr_spec == 'See': wr_spec = 'SEE'
    elif wr_spec == 'Basic Po': wr_spec = 'Basic PO'
    elif wr_spec == 'Senior Po': wr_spec = 'Senior PO'

    return wr_spec

def create_wr_obj(ksa, ksa_key, wr_item, key):
    item = {}
    item['ID'] = key
    item['KSA_ID'] = wr_item['KSA']
    item['proficiency'] = wr_item['proficiency']
    item['training'] = ', '.join(ksa['training'])
    item['eval'] = ', '.join(ksa['eval'])
    item['parent'] = ', '.join(ksa['parent'])
    item['children'] = ', '.join(ksa['children'])
    item['description'] = ksa['description']
    item['topic'] = ksa['topic']
    item['requirement_src'] = ', '.join(ksa['requirement_src'])
    item['requirement_owner'] = ksa['requirement_owner']

    return item

def main():
    global error
    global log_str
    root = '.frontend/assets/data/tables'
    ksadata = {}
    wrdata = {}
    ttl_list = []

    with open('requirements/TASKS.json', 'r') as tasksfile:
        ksadata = json.load(tasksfile)
    with open('requirements/KNOWLEDGE.json', 'r') as knowledgefile:
        ksadata = {**json.load(knowledgefile), **ksadata}
    with open('requirements/SKILLS.json', 'r') as skillfile:
        ksadata = {**json.load(skillfile), **ksadata}
    with open('requirements/ABILITIES.json', 'r') as abilityfile:
        ksadata = {**json.load(abilityfile), **ksadata}

    os.makedirs(root, exist_ok=True)

    rootdir = '.'
    for dirName, subdirlist, filelist in os.walk(rootdir):
        if dirName in ttl_dirs:
            for fname in filelist:
                ttl_list.append(os.path.abspath(f'{dirName}/{fname}'))

    for ttl in ttl_list:
        file_data = {}
        wr_spec = get_wr_spec(ttl)

        with open(ttl, 'r') as ttlfile:
            ttldata = json.load(ttlfile)
            file_name = ttl.split('/')[-1].split('.')[0]
            for key in ttldata:
                ksa_id = ttldata[key]['KSA']
                try:
                    ksa = ksadata[ttldata[key]['KSA']]
                    wr_item = ttldata[key]
                    try:
                        wrdata[file_name]['KSA_list'].append(ksa_id)
                    except:
                        wrdata[file_name] = {
                            'Workrole/Specialization': wr_spec,
                            'KSA_list': [ksa_id],
                        }
                    file_data[key] = create_wr_obj(ksa, ksa_id, wr_item, key)
                except:
                    error = 1
                    log_str = log_str + f'{ksa_id} not found in KSAT json: {key} in {ttl}\n'

            with open(f'{root}/{file_name}.min.json', 'w+') as fd:
                json.dump(list(file_data.values()), fd, sort_keys=False, separators=(',', ':'))

    for key in wrdata.keys():
        wrdata[key]['KSA_list'] = ', '.join(wrdata[key]['KSA_list'])
    
    with open(f'{root}/wr_spec.min.json', 'w') as fd:
        json.dump(list(wrdata.values()), fd, sort_keys=False, separators=(',', ':'))

    if error:
        with open(f'{root}/create_ttl_dataset.log', 'w+') as log_file:
            log_file.write(log_str) 
        print(f'There are errors present. Read the \'{root}/create_ttl_dataset.log\' artifact file')

if __name__ == "__main__":
    main()