#!/usr/bin/python3

import os, json, sys

def main():
    if len(sys.argv) < 2:
        print('\n\t./clear_trn_evl_fields [PATH]\n')
        exit(1)

    path = sys.argv[1]

    fin_data = {}

    with open(path, 'r') as datafile:
        data = json.load(datafile)

        for key in data.keys():
            try:
                fin_data[key] = data[key]
                fin_data[key]['eval'] = []
                fin_data[key]['training'] = []
            except:
                continue

    with open(path, 'w') as datafile:
        json.dump(fin_data, datafile, sort_keys=False, indent=4)

if __name__ == "__main__":
    main()
