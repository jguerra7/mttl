import os, json

def get_all_json(root, identifier, avoid_list):
    json_list = []

    for dirName, subdirlist, filelist in os.walk(root):
        for fname in filelist:
            if identifier in fname and fname not in avoid_list:
                json_list.append(os.path.join(dirName, fname))
    return json_list


def get_requirement_dict(file_path: str) -> dict:
    '''
    Purpose: Generates a Python dictionary version of a requirement file, which is expected to be a JSON file.
    :param file_path: The path to the requirement file to convert to a dictionary
    :return: Upon success: json_data = a dict of the requested requirements
             Upon failure:
                raise ValueError = the input param 'file_path' doesn't exist
                raise json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError, Exception,
                    BaseException = the input param 'file_path' cannot be loaded into a dictionary
                {} = Data Error
    '''
    if not os.path.exists(file_path):
        msg = "Is '{0}' the correct path?.".format(file_path)  # Prevent exposing variable name in stack trace
        raise ValueError(msg)

    try:
        json_data = json.load(open(file_path, "r"))

    except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError, Exception, BaseException) as e:
        raise ValueError("Unable to build requirements: {0}".format(e))

    return json_data


def get_all_requirements(req_files: list) -> dict:
    '''
    Purpose: Generates a single dictionary of all requirements.
    :param req_files: A list of file paths to the requirement files
    :return: Upon success: reqs = a dict of all requirements
             Upon failure:
                raise ValueError = an empty list of requirements files is received or a requirement file doesn't exist
                raise json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError, Exception,
                    BaseException = one or more requirements files cannot be loaded into a dictionary
                {} = Data Error
    '''
    reqs = {}

    if len(req_files) == 0:
        # In case the file list couldn't be generated
        raise ValueError("Error building requirements: input file list was empty.")

    for req_file in req_files:
        try:
            reqs.update(get_requirement_dict(req_file))

        except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError, ValueError,
                Exception, BaseException):
            raise

    return reqs
