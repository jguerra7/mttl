#!/usr/bin/python3

import os
import re
import sys
import json
import helpers
import argparse
import jsonschema

avoid_list_default = None
avoid_list_desc = "A list of files to ignore when building the working file list. " \
                  "The default is '{0}'.".format(avoid_list_default)
description = "This validates all Training Task List (TTL) files, which includes specialties as well as work roles."
evl_urls_file_default = "EVL.json"
evl_urls_file_desc = "The absolute or relative path to the file name containing the evaluation repo URLs. " \
                     "The default is {0}.".format(evl_urls_file_default)
keyword_req_default = ".json"
keyword_req_desc = "A keyword to specify the returned requirement file paths used during validation. " \
                   "The default is '{0}'.".format(keyword_req_default)
keyword_ttl_default = ".json"
keyword_ttl_desc = "A keyword to specify the returned Training Task List (TTL) file paths for processing. " \
                   "The default is '{0}'.".format(keyword_ttl_default)
log_file_default = "ValidateTTLLog.json"
log_file_desc = "The name of the log file; the default is '{0}'".format(log_file_default)
out_dir_default = "."
out_dir_desc = "The directory to store the output 'log_file'. Note, the directory is created if it doesn't exist. " \
               "By default files are written to {0}.".format(out_dir_default)
start_dirs_default = ["./workroles", "./specializations"]
start_dirs_desc = "These are the starting directories to the training task list (TTL) files. " \
                  "The default is '{0}'.".format(start_dirs_default)
req_files_dir_default = "./requirements"
req_files_dir_desc = "The absolute or relative path to the directory with the requirements. " \
                     "The default is {0}.".format(req_files_dir_default)

parser = argparse.ArgumentParser(description=description)
parser.add_argument("-a", "--avoid_list", type=list, default=avoid_list_default, help=avoid_list_desc)
parser.add_argument("-K", "--keyword_req", type=str, default=keyword_req_default, help=keyword_req_desc)
parser.add_argument("-k", "--keyword_ttl", type=str, default=keyword_ttl_default, help=keyword_ttl_desc)
parser.add_argument("-l", "--log_file", type=str, default=log_file_default, help=log_file_desc)
parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
parser.add_argument("-s", "--start_dirs", type=list, default=start_dirs_default, help=start_dirs_desc)
parser.add_argument("-r", "--req_files_dir", type=str, default=req_files_dir_default, help=req_files_dir_desc)

log = []
# This represents the general keys/values required in a Training Task List (TTL) JSON file
ttl_schema = {
    "title": "Schema for a MTTL Training Task List (TTL) JSON file.",
    "type": "object",
    "patternProperties": {
        "^[A-Za-z]+[0-9]{4,}$": {
            "type": "object",
            "properties": {
                "KSA": {"type": "string", "pattern": "^[A-Za-z]+[0-9]{4,}$"},
                "proficiency": {"type": "string", "minLength": 0}
            },
            "required": [
                "KSA",
                "proficiency"
            ],
            "additionalProperties": False
        }
    },
    "minProperties": 1,
    "additionalProperties": False
}


def validate_ksat(valid_ksats: list, ksat: str) -> list:
    '''
    Purpose: This validates a single KSAT ID against a known list of valid KSATS.
    :param valid_ksats: The list of valid KSATs
    :param ksat: The KSAT needing validation.
    :return: Success = []; otherwise, a list of errors
    '''
    ksat_log = []

    if ksat not in valid_ksats:
        ksat_log.append("KSAT ID {0} is not a valid requirement.".format(ksat))
    return ksat_log


def valid_prof_code(prof_code: str) -> bool:
    '''
    Purpose: This function validates the proficiency code of the input this_json file. If it's valid then an empty
    dictionary is returned; otherwise, a dictionary of errors is returned.
    :param prof_code: The proficiency code stored in file parameter
    :return: False = The proficiency code is invalid; True = The proficiency code is valid
    '''
    valid_prof_codes = ['1a', '1b', '1c', '1d', '2a', '2b', '2c', '2d', '3a', '3b', '3c', '3d', '4a', '4b', '4c', '4d',
                        '0', '1', '2', '3', '4', 'a', 'b', 'c', 'd', 'A', 'B', 'C', 'D']
    if prof_code not in valid_prof_codes:
        # In case the proficiency code isn't valid
        return False
    return True


def validate_ttl_entries(valid_ksats: list, ttl_data: dict, ttl_file: str) -> list:
    '''
    Purpose: This validates each TTL entry to ensure each entry's listed KSAT and proficiency code is valid.
    :param valid_ksats: A list representing the valid KSAT requirements.
    :param ttl_data: A dictionary object representing a Training Task List (TTL).
    :param ttl_file: A string representing the name of the Training Task List (TTL) file.
    :return: Success = []; otherwise, a list of errors
    '''
    ksat_log = []

    try:
        for task_id, task_info in ttl_data.items():
            ksat_code = task_info["KSA"]
            prof_code = task_info["proficiency"]
            this_log = validate_ksat(valid_ksats, ksat_code)
            if re.search("^[KSA][0-9]{4,}$", ksat_code.upper()) and not valid_prof_code(prof_code):
                this_log.append("'{0}' is an invalid proficiency code.".format(prof_code))
            if re.search("^[T][0-9]{4,}$", ksat_code.upper()) and len(prof_code) > 0:
                this_log.append("Tasks do not have proficiency codes; enter as \"\".")
            if len(this_log) > 0:
                ksat_log.append("ERROR in {0}: {1}".format(task_id, this_log))
    except (KeyError, ValueError) as err:
        ksat_log.append("Error parsing '{0}': {1}".format(ttl_file, err))
    return ksat_log


def validate_ttl_file(ttl_data: dict, ttl_file: str) -> list:
    '''
    Purpose: This function validates a single training task list (TTL) JSON file against its expected schema.
    :param ttl_data: A dictionary representing a specific training task list (TTL) JSON file
    :param ttl_file: The name of the training task list (TTL) JSON file
    :return: Success = []; otherwise, a list of errors
    '''
    global ttl_schema
    ttl_log = []

    try:
        # Allow collecting all validation errors by using separate try blocks
        jsonschema.validate(instance=ttl_data, schema=ttl_schema)  # Check requirement file generally
    except jsonschema.ValidationError as e:
        ttl_log.append("Validation Error(s) in {0}: {1}".format(os.path.basename(ttl_file), e))
    return ttl_log


def main() -> int:
    '''
    Purpose: This is responsible for collecting all requested input as well as valid data for comparison in order to
    validate each requirement file within the MTTL. Error statements are kept in a list called log.
    :return: Success = 0; errors = 1
    '''
    global args, log
    ttl_files = []

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    if args.avoid_list is None:
        args.avoid_list = []

    try:
        req_files = helpers.get_all_json(args.req_files_dir, args.keyword_req, args.avoid_list)
    except (FileExistsError, FileNotFoundError, OSError, IOError) as err:
        log.append("Unable to determine requirement files: {0}".format(err))
        return 1

    try:
        valid_ksats = helpers.get_all_requirements(req_files)
    except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError, ValueError,
            Exception, BaseException) as err:
        log.append("Unable to generate valid KSAT data: {0}".format(err))
        return 1

    try:
        for ttl_dir in args.start_dirs:
            ttl_files += helpers.get_all_json(ttl_dir, args.keyword_ttl, args.avoid_list)
    except (FileExistsError, FileNotFoundError, OSError, IOError) as err:
        log.append("Unable to determine Training Task List (TTL) files: ".format(err))
        return 1

    for ttl_file in ttl_files:
        try:
            with open(ttl_file, "r") as req_fp:
                ttl_data = json.load(req_fp)
        except json.JSONDecodeError as err:
            log.append("Unable to load JSON data in {0}: {1}.".format(ttl_file, err))
        else:
            # Validate this Training Task List (TTL) JSON only if the json.load was successful
            log += validate_ttl_file(ttl_data, ttl_file)
            # Validate the KSAT mappings within each requirement to ensure they're valid.
            log += validate_ttl_entries(list(valid_ksats.keys()), ttl_data, ttl_file)

    if len(log) > 0:
        return 1
    else:
        return 0


if __name__ == "__main__":
    args = parser.parse_args()
    status = main()
    if status == 0:
        msg = "All training task lists (TTLs) are valid."
        print(msg)
        log.append(msg)
    else:
        print("Check {0} for validation errors.".format(os.path.join(args.out_dir, args.log_file)))

    with open(os.path.join(args.out_dir, args.log_file), "w") as log_fp:
        json.dump(log, log_fp, indent=4)

    sys.exit(status)
